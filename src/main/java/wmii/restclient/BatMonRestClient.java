package wmii.restclient;

import android.util.Log;

import org.springframework.http.converter.FormHttpMessageConverter;
import org.springframework.http.converter.StringHttpMessageConverter;
import org.springframework.util.LinkedMultiValueMap;
import org.springframework.util.MultiValueMap;
import org.springframework.web.client.HttpServerErrorException;
import org.springframework.web.client.RestClientException;
import org.springframework.web.client.RestTemplate;

import java.util.HashMap;
import java.util.List;

import wmii.applicationscanner.Application;

public class BatMonRestClient implements RestClient<Application> {

    RestTemplate restTemplate;
    String url;

    public BatMonRestClient(String url) {
        this.url = url;
        this.restTemplate = new RestTemplate();
        this.restTemplate.getMessageConverters().add(new StringHttpMessageConverter());
        this.restTemplate.getMessageConverters().add(new FormHttpMessageConverter());
    }


    @Override
    public String GET(String _id) {
        String urlForSpecificId = url + "/?package=" + _id;
        return restTemplate.getForObject(urlForSpecificId, String.class);
    }

    @Override
    public String GET() {
        String urlForAllObjects = url + "/?package=ANY";
        return restTemplate.getForObject(urlForAllObjects, String.class);
    }

    @Override
    public String POST(Application item) {

        MultiValueMap<String, String> map = new LinkedMultiValueMap<String, String>();
        map.add("package", item.get_id());
        map.add("name", item.getName());
        map.add("permissions", item.getPermissions().toString().replaceAll("\\[", "").replaceAll("\\]",""));
        map.add("mobileRxFg", String.valueOf(item.getMobileRxFg()));
        map.add("mobileRxBg", String.valueOf(item.getMobileRxBg()));
        map.add("wlanRxFg", String.valueOf(item.getWlanRxFg()));
        map.add("wlanRxBg", String.valueOf(item.getWlanRxBg()));
        map.add("num", "1");
        return restTemplate.postForObject(url, map, String.class);
    }

    @Override
    public HashMap<String, String> POST(List<Application> items) {
        HashMap<String, String> response = new HashMap<>();
        for (Application item : items) {
            MultiValueMap<String, String> map = new LinkedMultiValueMap<String, String>();
            map.add("package", item.get_id());
            map.add("name", item.getName());
            map.add("permissions", item.getPermissions().toString().replaceAll("\\[", "").replaceAll("\\]", ""));
            map.add("mobileRxFg", String.valueOf(item.getMobileRxFg()));
            map.add("mobileRxBg", String.valueOf(item.getMobileRxBg()));
            map.add("wlanRxFg", String.valueOf(item.getWlanRxFg()));
            map.add("wlanRxBg", String.valueOf(item.getWlanRxBg()));
            map.add("num", "1");
            try {
                response.put(item.get_id(), restTemplate.postForObject(url, map, String.class));
            } catch (RestClientException e) {
                Log.d("POST", e.toString());
            }
        }
        return response;
    }

    @Override
    public void PUT(Application item) {
        MultiValueMap<String, String> map = new LinkedMultiValueMap<String, String>();
        map.add("package", item.get_id());
        map.add("mobileRxFg", String.valueOf(item.getMobileRxFg()));
        map.add("mobileRxBg", String.valueOf(item.getMobileRxBg()));
        map.add("wlanRxFg", String.valueOf(item.getWlanRxFg()));
        map.add("wlanRxBg", String.valueOf(item.getWlanRxBg()));
        try{
            restTemplate.put(url, map, String.class);
        } catch (HttpServerErrorException e) {
            Log.d("http", item.get_id());
        }
    }

    @Override
    public void PUT(String _id, long mobileRxFg, long mobileRxBg, long wlanRxFg, long wlanRxBg) {
        MultiValueMap<String, String> map = new LinkedMultiValueMap<String, String>();
        map.add("package", _id);
        map.add("mobileRxFg", String.valueOf(mobileRxBg));
        map.add("mobileRxBg", String.valueOf(mobileRxBg));
        map.add("wlanRxFg", String.valueOf(wlanRxFg));
        map.add("wlanRxBg", String.valueOf(wlanRxBg));
        try{
            restTemplate.put(url, map, String.class);
        } catch (HttpServerErrorException e) {
            Log.d("http", _id);
        }
    }
}
