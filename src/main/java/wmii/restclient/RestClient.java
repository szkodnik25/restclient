package wmii.restclient;

import java.util.HashMap;
import java.util.List;

public interface RestClient<T> {

    String GET(String _id);
    String GET();

    String POST(T item);
    HashMap<String, String> POST(List<T> items);

    void PUT(T item);
    void PUT(String _id, long mobileRxFg, long mobileRxBg, long wlanRxFg, long wlanRxBg);
}
